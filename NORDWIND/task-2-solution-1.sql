/*
Write SQL query to find all contact persons 
with the last name beginning with 'L' 
(the first and last name must be one column)
*/

SELECT CustomerID, ContactName, CompanyName
FROM (
    SELECT
        CONCAT_WS( " ", first_name, last_name ) AS ContactName,
        id AS CustomerID,
        company AS CompanyName
    FROM customers
) AS ContactNames
WHERE ContactName LIKE '% L%';