/*
Write SQL query to find all contact persons 
with the last name beginning with 'L' 
(the first and last name must be one column)
*/

SELECT 
    id AS CustomerID,
    CONCAT_WS( " ", first_name, last_name ) AS ContactName,  
    company AS CompanyName 
FROM customers
WHERE CONCAT_WS( " ", first_name, last_name ) LIKE '% L%';