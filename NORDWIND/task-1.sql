/*
List of all employees whose last name are containing two letters 'e' (anywhere)
*/

SELECT First_Name, Last_Name FROM employees
WHERE Last_Name LIKE '%e%e%';