SELECT TP.OrderID, TP.suma, O.CustomerID
FROM (
    SELECT 
        OrderID, SUM(UnitPrice*Quantity) AS suma
    FROM 
        `Order Details`
    GROUP BY OrderID
    ) AS TP
JOIN Orders as O on TP.OrderID = O.OrderID
WHERE suma >= 10000
ORDER BY suma DESC;