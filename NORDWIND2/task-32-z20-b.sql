SELECT  P.ProductID, ProductName, COUNT(OrderID) AS Zamowienia
FROM Products as P
JOIN `Order Details` AS OD ON P.ProductID = OD.ProductID
GROUP BY P.ProductID
HAVING COUNT(OrderID) > 40
ORDER BY Zamowienia DESC