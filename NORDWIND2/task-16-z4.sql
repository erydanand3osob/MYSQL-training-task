SELECT OrderID, COUNT(*) AS ilosc 
FROM `order details`
GROUP BY OrderID
HAVING  ilosc >= 5
ORDER BY  ilosc DESC;
