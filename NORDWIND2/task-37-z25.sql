SELECT CompanyName, ContactName, CONCAT(Phone, " Fax: ",Fax) as Contact
FROM Customers
WHERE Country = 'UK'
HAVING Contact IS NOT NULL