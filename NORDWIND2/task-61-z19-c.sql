
SELECT DISTINCT Country, City
FROM Employees


UNION

SELECT Country, City
FROM Customers
WHERE
    (NOT EXISTS
    (SELECT Country FROM Suppliers))
    AND
    (NOT EXISTS 
    (SELECT City FROM Suppliers))