SELECT EmployeeID, 

CASE TitleOfCourtesy
WHEN 'Ms.' THEN 'Pani'
WHEN 'Mr.' THEN 'Pan'
ELSE 
TitleOfCourtesy
END
AS Tytul,

CONCAT(FirstName," " , LastName) AS Pracownik
FROM Employees
WHERE EmployeeID IN(1, 2, 3, 6, 8, 9)