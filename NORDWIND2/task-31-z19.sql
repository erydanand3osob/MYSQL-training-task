SELECT C.CustomerID, CompanyName, SUM(Quantity * UnitPrice) as Suma
FROM Customers AS C
JOIN Orders AS O ON C.CustomerID = O.CustomerID
JOIN `Order Details` AS OD ON O.OrderID = OD.OrderID
GROUP BY C.CustomerID
ORDER BY Suma DESC
LIMIT 5