UPDATE ProductDiscount
SET Discount = 0.09
where ProductID IN 
(SELECT * FROM
    (   SELECT ProductID
        FROM ProductDiscount
        ORDER BY UnitPrice DESC
        LIMIT 10
    ) T
)
