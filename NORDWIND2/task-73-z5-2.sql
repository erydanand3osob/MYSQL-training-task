CREATE TABLE  ARCHIWUMPD (
    ProductID INT,
    OldDiscount DECIMAL(4,3) DEFAULT(0),
    NewDiscount DECIMAL(4,3) DEFAULT(0),
    PRIMARY KEY(ProductID)
    );

