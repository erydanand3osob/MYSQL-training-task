SELECT DISTINCT FirstName, DENSE_RANK() OVER(ORDER BY FirstName) as nr
FROM employees
ORDER BY FirstName;