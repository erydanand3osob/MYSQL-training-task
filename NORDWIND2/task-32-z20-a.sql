SELECT  ProductID, ProductName, Zamowienia
FROM(
SELECT  P.ProductID, ProductName, COUNT(OrderID) AS Zamowienia
FROM Products as P
JOIN `Order Details` AS OD ON P.ProductID = OD.ProductID
GROUP BY P.ProductID
) as sprzedaz
WHERE Zamowienia > 40
ORDER BY Zamowienia DESC