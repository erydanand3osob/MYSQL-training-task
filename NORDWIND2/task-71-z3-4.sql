USE northwind;

DELETE FROM Zabawki
WHERE Cena IN(
    SELECT CenaMAX
    FROM (
        SELECT MAX(Cena) as CenaMAX FROM Zabawki
        GROUP BY Kategoria
    ) C
);