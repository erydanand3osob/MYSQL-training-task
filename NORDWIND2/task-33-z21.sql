USE northwind2;
set @lp=0;
SELECT O.CustomerID, O.OrderID, SUM(OD.UnitPrice*OD.Quantity) AS OrderValue, @lp:=@lp+1  AS nr 
FROM Orders AS O
JOIN `Order Details` AS OD ON O.OrderID = OD.OrderID
WHERE O.CustomerID IN('ALFKI', 'ISLAT', 'SAVEA')
GROUP BY O.OrderID
ORDER BY nr, O.CustomerID, OrderValue DESC;