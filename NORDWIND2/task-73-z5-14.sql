WITH CTE AS (
SELECT PD.Discount as Disc, APD.OldDiscount as OldDisc
FROM ProductDiscount as PD LEFT JOIN ArchiwumPD as APD
on PD.ProductID = APD.ProductID
ORDER BY UnitPrice DESC
LIMIT 6 OFFSET 6 
)

UPDATE CTE
SET Disc = OldDisc;

#Do zrobienia