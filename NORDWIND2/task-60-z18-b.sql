SELECT 'Customer' AS 'Type' ,CompanyName, Country, City
FROM Customers

UNION ALL

SELECT 'Supplier' AS 'Type', CompanyName, Country, City
FROM Suppliers

ORDER BY Type, Country, City