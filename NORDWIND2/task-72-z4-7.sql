USE Northwind;

SELECT CustomerID, OrderID, ShipDelay
FROM Ord2
WHERE ShipDelay > 0
AND EXISTS (
SELECT *
FROM Ord2 AS OC
LEFT JOIN Ord2 AS O ON OC.CustomerID= O.CustomerID
AND O.OrderID > OC.OrderID
)
order by CustomerID, OrderID;