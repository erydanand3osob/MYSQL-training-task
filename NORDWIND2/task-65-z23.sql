SELECT CompanyName, OrderID, SaleAmount,
CAST(100. * SaleAmount / SUM(SaleAmount) OVER(PARTITION BY CompanyName) as DECIMAL(5,2)) as 'Percent',
SUM(SaleAmount) OVER(PARTITION BY CompanyName) as TotalByCost
FROM `Sales Totals by Amount`
ORDER BY CompanyName;