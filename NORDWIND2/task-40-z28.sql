use northwind2;

SELECT 
NazwaFirmy, Rok, Zamownienia, Nr FROM
(SELECT S.CompanyName as NazwaFirmy, COUNT(*) as Zamownienia, YEAR(OrderDate) as Rok,
    @curRank := IF(@prevRank = COUNT(*) , @curRank, @incRank) AS Nr, 
    @incRank := @incRank + 1, 
    @prevRank := COUNT(*)

    FROM Orders as O
        JOIN Shippers as S ON O.Shipvia = S.ShipperID, 
        (
        SELECT @curRank :=0, @prevRank  := NULL, @incRank := 1
        ) r 
        
group by NazwaFirmy, Rok
) s
order by  Nr, NazwaFirmy

