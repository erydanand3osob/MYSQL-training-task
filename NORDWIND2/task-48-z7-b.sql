SELECT SupplierID, CompanyName 
FROM Suppliers as S 
WHERE S.SupplierID IN (
    SELECT P.SupplierID FROM Products as P WHERE P.ProductID IN (
        SELECT OD.ProductID FROM `Order details` as OD WHERE  OD.OrderID = 10337
    ))
ORDER BY S.SupplierID
