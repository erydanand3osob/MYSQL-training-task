SELECT O.CustomerID, MIN(OrderID) as FirstOrder, MAX(OrderID) as LastOrder
FROM Orders as O
Group by O.CustomerID
Having CustomerID IN ( SELECT CustomerID FROM Customers WHERE  Country = 'Germany')