SET @miasto = 'Berlin';

WITH SuppliersByCity AS
(
SELECT SupplierID, CompanyName, City, Country
FROM Suppliers
WHERE city = @miasto
)
SELECT * FROM SuppliersByCity;