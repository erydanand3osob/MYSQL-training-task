SELECT P.SupplierID, S.CompanyName, ProductName
FROM Products as P
JOIN Suppliers as S ON P.SupplierID = S.SupplierID
WHERE ProductName = 'Tofu'