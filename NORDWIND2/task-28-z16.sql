SELECT DISTINCT E.EmployeeID, FirstName, LastName, Title, COUNT(OrderID) AS Orders
FROM Employees AS E
JOIN Orders AS O ON E.EmployeeID = O.EmployeeID
GROUP BY O.EmployeeID
ORDER BY  Orders DESC;