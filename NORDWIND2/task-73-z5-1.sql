CREATE TABLE  ProductDiscount (
    ProductID INT,
    UnitPrice INT NOT NULL,
    Discount DECIMAL(4,3) DEFAULT(0),
    PRIMARY KEY(ProductID)
    );

