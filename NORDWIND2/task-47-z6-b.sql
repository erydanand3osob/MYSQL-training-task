SELECT OrderID, CustomerID, EmployeeID
FROM Orders 
WHERE (EmployeeID IN( SELECT EmployeeID FROM Employees WHERE Title = 'Sales Representative')) AND
(CustomerID IN( SELECT CustomerID FROM Customers WHERE Country = 'USA'))
ORDER BY CustomerID, OrderID;
