SET @sum = 500;

SELECT OrderID, Subtotal
FROM `Order Subtotals`
WHERE Subtotal  BETWEEN 0.9*@sum AND 1.1*@sum 
ORDER BY Subtotal
