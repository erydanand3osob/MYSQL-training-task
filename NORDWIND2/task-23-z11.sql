Use northwind2;

SELECT  YEAR(OrderDate) as Rok, C.CategoryName , COUNT(O.OrderID) as Zamowienia
FROM Orders AS O 
JOIN `Order Details` AS OD ON O.OrderID = OD.OrderID
JOIN Products as P ON OD.ProductID =  P.ProductID
JOIN Categories AS C ON P.CategoryID = C.CategoryID
GROUP BY Rok, C.CategoryName