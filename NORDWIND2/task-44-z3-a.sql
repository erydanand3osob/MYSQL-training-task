SELECT CustomerID, OrderDate
FROM Orders
GROUP BY CustomerID
HAVING MIN(OrderDate) >= '19970101';