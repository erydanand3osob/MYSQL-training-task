SELECT O.OrderID, C.CompanyName, OS.Subtotal
FROM Orders AS O 
LEFT JOIN Customers AS C ON O.CustomerID = C.CustomerID
LEFT JOIN `Order Subtotals` AS OS ON O.OrderID = OS.OrderID
Order by  O.OrderID