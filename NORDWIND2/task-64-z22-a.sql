SELECT FirstName, ROW_NUMBER() OVER(ORDER BY FirstName) as nr
FROM employees
GROUP BY FirstName
ORDER BY FirstName;