SELECT FirstName, ROW_NUMBER() OVER(ORDER BY FirstName) as nr
FROM ( SELECT DISTINCT FirstName FROM employees ) as Tab
ORDER BY FirstName;