SELECT EmployeeID, count(OrderID) as ilosc,

CASE
WHEN COUNT(OrderID) > 100 THEN 'więcej niż 100'
WHEN COUNT(OrderID) BETWEEN 50 AND 100 THEN 'między 50 a 100'
WHEN COUNT(OrderID) < 50 THEN 'mniej niż 50'
END
AS zamowienia

FROM Orders
GROUP BY EmployeeID;