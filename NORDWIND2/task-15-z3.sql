SELECT OrderID, SUM(UnitPrice*Quantity) AS Suma
FROM `order details`
GROUP BY OrderID
ORDER BY  Suma DESC
LIMIT 3;
