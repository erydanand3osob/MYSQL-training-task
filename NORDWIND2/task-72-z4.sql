SELECT CustomerID
FROM Ord2
WHERE ShipDelay > 0
AND EXISTS (
SELECT *
FROM Ord2 as O
WHERE Ord2.CustomerID = O.CustomerID
AND O.OrderID > Ord2.OrderID
)
AND (CustomerID IN (
SELECT CustomerID
FROM Ord2
WHERE ShipDelay > 0
AND NOT EXISTS (
SELECT *
FROM Ord2 as O
WHERE Ord2.CustomerID = O.CustomerID
AND O.OrderID > Ord2.OrderID
)));