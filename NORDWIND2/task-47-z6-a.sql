SELECT OrderID, C.CustomerID, E.EmployeeID
FROM Orders as O
JOIN Employees as E ON O.EmployeeID = E.EmployeeID
JOIN Customers as C ON O.CustomerID = C.CustomerID
WHERE C.Country = 'USA' AND E.Title = 'Sales Representative'
order by C.CustomerID