WITH SuppliersByCity  AS (
    SELECT 
        SupplierID, CompanyName, City, Country
    FROM 
        Suppliers
    WHERE 
        City = 'London'
)
SELECT * FROM SuppliersByCity;