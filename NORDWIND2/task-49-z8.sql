SELECT CustomerID
FROM Orders AS O 
WHERE O.OrderID IN (SELECT OD.OrderID FROM `Order details` as OD WHERE  ProductID = 28)
GROUP BY CustomerID
ORDER BY CustomerID
