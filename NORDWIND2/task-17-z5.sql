SELECT  C.CustomerID, C.CompanyName, COUNT(O.OrderID) as Zamowienia
FROM   Customers as C
LEFT JOIN Orders as O ON O.CustomerID = C.CustomerID
GROUP BY C.CustomerID, C.CompanyName
ORDER BY Zamowienia DESC;