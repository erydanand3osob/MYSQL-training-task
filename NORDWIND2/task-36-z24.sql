SELECT CompanyName, ContactName, Phone
FROM Customers
WHERE fax IS NULL
HAVING Phone IS NOT NULL
ORDER BY CompanyName;