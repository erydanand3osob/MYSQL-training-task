SELECT OD.OrderID, COUNT(*) as Produkty, AVG(OD.UnitPrice* OD.Quantity) as Srednia, O.ShipCountry
FROM orders as O
JOIN `order details` as OD ON OD.OrderID = O.OrderID
GROUP BY O.OrderID
HAVING O.OrderID IN(10250, 10657, 10710, 10901);