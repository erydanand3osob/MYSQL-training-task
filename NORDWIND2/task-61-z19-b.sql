SELECT Country, City
FROM Customers

UNION

SELECT DISTINCT Country, City
FROM Suppliers

WHERE
    Country NOT IN 
    (SELECT Country FROM Employees)
    AND
    City NOT IN 
    (SELECT City FROM Employees)

