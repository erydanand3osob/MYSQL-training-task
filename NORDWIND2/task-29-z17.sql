SELECT DISTINCT E.EmployeeID, LastName, Title, SUM(UnitPrice * Quantity) as Sprzedaz
FROM Employees AS E
JOIN  Orders as O ON E.EmployeeID = O.EmployeeID
JOIN  `Order Details` as OD ON O.OrderID = OD.OrderID
WHERE YEAR(OrderDate) = 1997
GROUP BY E.EmployeeID 
ORDER BY Sprzedaz DESC