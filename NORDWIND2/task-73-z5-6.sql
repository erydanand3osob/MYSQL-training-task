CREATE TRIGGER before_employee_update 
    BEFORE UPDATE ON ProductDiscount
    FOR EACH ROW 
BEGIN
    INSERT INTO archiwumpd
    SET ProductID = OLD.ProductID,
        OldDiscount = OLD.Discount,
        NewDiscount = NEW.Discount
END ;
#CREATE with phpmyadmin