SET @miasto = 'sa';

WITH SuppliersByCity AS
(
SELECT SupplierID, CompanyName, City, Country
FROM Suppliers
WHERE city LIKE  CONCAT('%', @miasto, '%')
)
SELECT * FROM SuppliersByCity;