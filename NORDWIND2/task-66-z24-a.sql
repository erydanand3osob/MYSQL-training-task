SELECT CompanyName, SaleAmount,
SUM(SaleAmount) OVER(PARTITION BY CompanyName) as TotalByCost,
SUM(SaleAmount) OVER() as 'Total'

FROM `Sales Totals by Amount`
ORDER BY CompanyName;