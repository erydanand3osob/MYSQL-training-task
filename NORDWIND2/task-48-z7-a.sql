SELECT S.SupplierID, CompanyName 
FROM Suppliers as S
JOIN Products as P ON S.SupplierID = P.SupplierID
JOIN `Order details` as OD ON P.ProductID = OD.ProductID
JOIN Orders as O ON OD.OrderID = O.OrderID
WHERE OD.OrderID = 10337
GROUP BY CompanyName 
ORDER BY S.SupplierID