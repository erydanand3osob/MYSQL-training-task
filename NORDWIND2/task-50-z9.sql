SELECT OS1.OrderID, OS1.Subtotal
FROM `Order Subtotals` as OS1
WHERE OS1.Subtotal >= 3*(
    SELECT 
        AVG(OS2.Subtotal) 
    FROM 
        `Order Subtotals` AS OS2)
ORDER BY OS1.OrderID