UPDATE ProductDiscount
SET Discount = 0.06
where ProductID IN 
(SELECT * FROM
    (   SELECT ProductID
        FROM ProductDiscount
        ORDER BY UnitPrice DESC
        LIMIT 5 OFFSET 10
    ) T
)
