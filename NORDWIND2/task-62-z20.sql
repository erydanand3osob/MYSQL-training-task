With PM As (
    SELECT Distinct 
    City, Country
    FROM  Suppliers
   INNER JOIN customers using(City, Country)
)

SELECT  s.SupplierID, s.CompanyName, s.City, c.CustomerID, c.CompanyName
FROM Suppliers as s 
INNER JOIN customers as c on s.city = c.city
WHERE 
    s.City 
    IN (SELECT City from PM)
    AND
    s.Country 
    IN (SELECT Country from PM)