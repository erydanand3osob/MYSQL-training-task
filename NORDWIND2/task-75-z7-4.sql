CREATE VIEW Bank_widok AS
SELECT KL.ID, KL.Imie, KL.Nazwisko, KL.Miasto, KO.Saldo
FROM Bank_klienci KL inner join Bank_konta KO
ON KL.ID = KO.ID
WHERE KO.Saldo <= 1000 OR KO.Saldo >= 10000;