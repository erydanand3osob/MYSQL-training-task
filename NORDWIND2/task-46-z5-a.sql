SELECT O.CustomerID, MIN(OrderID) as FirstOrder, MAX(OrderID) as LastOrder
FROM Orders as O
JOIN Customers as C ON O.CustomerID = C.CustomerID
WHERE Country = 'Germany'
Group by O.CustomerID