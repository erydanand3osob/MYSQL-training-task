Use Northwind;

SELECT DISTINCT Country, City
FROM Customers
WHERE 
    Country IN 
    (SELECT Country FROM Suppliers)
    AND
    City IN 
    (SELECT City FROM Suppliers)

