SELECT DISTINCT O.CustomerID
FROM `order details` OD
JOIN Orders as O ON OD.OrderID = O.OrderID
WHERE ProductID = 1 
AND EXISTS (
SELECT *
FROM `Order Details` OD2 JOIN Orders O2
ON OD2.OrderID = O2.OrderID
WHERE O.CustomerID = O2.CustomerID
AND OD2.ProductID = 55
)
ORDER BY O.CustomerID;