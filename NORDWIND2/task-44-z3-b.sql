SELECT DISTINCT O.CustomerID
FROM Orders as O
WHERE O.CustomerID not in (SELECT o1.CustomerID FROM Orders as o1 WHERE o1.OrderDate >= '19960101' AND o1.OrderDate < '19970101')
AND O.CustomerID in (SELECT o2.CustomerID FROM Orders as o2 WHERE o2.OrderDate >= '19970101')
ORDER BY O.CustomerID;