SELECT O.CustomerID, OrderID, City
FROM Orders as O
JOIN Customers as C ON O.CustomerID = C.CustomerID
WHERE City = 'London'