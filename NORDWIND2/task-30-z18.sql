SELECT Country, CompanyName, ProductName   
FROM Suppliers AS S
JOIN Products AS P ON S.SupplierID = P.SupplierID
GROUP BY Country, CompanyName, ProductName
ORDER BY Country, CompanyName, ProductName   