USE northwind;

Create Table Zabawki (
id INT NOT NULL AUTO_INCREMENT,
KodProduktu INT NOT NULL,
NazwaProduktu VARCHAR(20) NOT NULL,
Cena INT NOT NULL,
Kategoria VARCHAR(15) NOT NULL,
PRIMARY KEY (id)
);