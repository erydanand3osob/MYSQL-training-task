SELECT o.OrderID,
(   SELECT 
        c.CompanyName 
    FROM 
        Customers as c 
    WHERE 
        o.CustomerID = c.CustomerID
) as 'CompanyName',
(   SELECT 
        os.Subtotal 
    FROM 
        `Order Subtotals` as os 
    WHERE 
        o.OrderID = os.OrderID 
) as 'Subtotal'
FROM Orders as o;