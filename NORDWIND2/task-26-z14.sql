SELECT YEAR(OrderDate) as rok , MONTH(OrderDate) as miesiac,  SUM(UnitPrice * Quantity) as Sprzedaz
FROM Orders AS O
JOIN `Order Details` AS OD ON O.OrderID = OD.OrderID
WHERE YEAR(OrderDate) = 1997
GROUP BY rok , miesiac;