WITH Tab AS (
SELECT o.OrderID, c.CompanyName, os.Subtotal
FROM Orders as o JOIN Customers as c on o.CustomerID = c.CustomerID
JOIN `Order Subtotals` as os on o.OrderID = os.OrderID
)

SELECT CompanyName, SUM(Subtotal) as `CustSum`, ( SELECT SUM(Subtotal) FROM `Order Subtotals` ) as TotalSum
FROM Tab
GROUP BY CompanyName
ORDER BY CustSum DESC
LIMIT 20;