WITH CTE AS
(SELECT DISTINCT CompanyName, 
SUM(SaleAmount) OVER(PARTITION BY CompanyName) as TotalByCost,
SUM(SaleAmount) OVER() as 'Total',
CAST(100. * SUM(SaleAmount) OVER(PARTITION BY CompanyName) / SUM(SaleAmount) OVER()  as DECIMAL(5,2)) as 'Percent'
FROM `Sales Totals by Amount`
ORDER BY CompanyName)

SELECT CompanyName, TotalByCost, Total, Percent, SUM(Percent) OVER(ORDER BY Percent DESC) as PercentSum
FROM CTE
Order by PercentSum;
