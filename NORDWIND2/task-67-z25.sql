WITH CTE AS (
SELECT Gender, COUNT(*) as num
FROM Employee
GROUP BY Gender
)

SELECT Gender, num, CAST(100. * num / SUM(num) OVER() as DECIMAL(5,2)) as Pct
FROM CTE;