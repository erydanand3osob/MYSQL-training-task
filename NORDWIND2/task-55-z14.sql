WITH ShortOrd  AS (
    SELECT 
        OrderID, CustomerID, EmployeeID, OrderDate, ShipVia
    FROM 
        Orders    
)

SELECT 
    SO.OrderID, SO.CustomerID, SUM(Subtotal)AS Suma, S.CompanyName as Shipper
FROM 
    ShortOrd as SO 
    JOIN Shippers as S ON SO.ShipVia = S.ShipperID
    JOIN `Order Subtotals` as OS ON SO.OrderID = OS.OrderID
    WHERE YEAR(SO.OrderDate) = '1996'
    GROUP BY  SO.OrderID
    ORDER BY  SO.CustomerID;
