SELECT Country, City, CompanyName
FROM customers
WHERE Country REGEXP '^[A-B]'
ORDER BY Country, City;