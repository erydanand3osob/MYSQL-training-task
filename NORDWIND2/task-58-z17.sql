Use Northwind;

WITH SalesSummary AS
(
    SELECT  ODE.OrderID, SaleAmount, CompanyName, ProductID, 
            ProductName, UnitPrice, Quantity, Discount, 
            ExtendedPrice, ShippedDate
    FROM    
        `order details extended` AS ODE 
        INNER JOIN `sales totals by amount` AS STBA ON ODE.OrderID = STBA.OrderID
)

SELECT * ,
 (SELECT 
    C.CustomerID 
    FROM 
        Customers AS C 
    WHERE 
        SalesSummary.CompanyName = C.CompanyName
 ) as CustomerID
FROM SalesSummary 
