SELECT YEAR(OrderDate) AS Rok, Month(OrderDate) AS Miesiac, Count(*) AS Zamowienia
FROM Orders
GROUP BY YEAR(OrderDate), MONTH(OrderDate);
